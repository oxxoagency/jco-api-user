package routes

import (
	"github.com/labstack/echo/v4"
	"jco-user-golang/controllers"
	"net/http"
	"github.com/labstack/echo/v4/middleware"
)

func Init() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.PUT("/member/password/reset", controllers.ResetPassword)
	e.PUT("/member/password/change", controllers.ChangePassword)
	e.GET("/member/me", controllers.Me)
	e.PUT("/member/update", controllers.UpdateMember)
	e.POST("/member/register", controllers.Register)
	e.POST("/member/login", controllers.LoginMember)
	e.GET("/member/logout", controllers.Logout)
	return e
}
