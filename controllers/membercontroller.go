package controllers

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gojektech/heimdall/v6/httpclient"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
	"io/ioutil"
	"jco-user-golang/config"
	"jco-user-golang/helpers"
	"jco-user-golang/models"
	"net/http"
	"os"
	"regexp"
	"time"
)

func Register(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))

	type DataBody struct {
		MemberName string `json:"member_name"`
		MemberEmail string `json:"member_email"`
		MemberPhone string `json:"member_phone"`
		MemberPhone2 string `json:"member_phone2"`
		MemberPassword string `json:"member_password"`
		MemberConfpassword string `json:"member_confpassword"`
		MemberIp string `json:"member_ip"`
		MemberUseragent string `json:"member_useragent"`
	}
	var databody DataBody
	err = json.Unmarshal(body, &databody)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	req, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/register", bytes.NewBuffer(body))
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	if res.StatusCode == 201 {
		var response models.ResponseRegister

		hashedPassword, _ := helpers.Hash(databody.MemberPassword)
		db := config.InitDatabase()
		memberdata := models.Register{
			MemberName: databody.MemberName,
			MemberEmail: databody.MemberEmail,
			MemberPhone: databody.MemberPhone,
			MemberPhone2: databody.MemberPhone2,
			MemberPassword: hashedPassword,
			MemberIp: databody.MemberIp,
			MemberUseragent: databody.MemberUseragent,
			CreatedAt: time.Now(),
		}

		//Insert data_member
		gormmember := db.Create(&memberdata)
		if gormmember.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		datamember := map[string]interface{}{}
		showmember := db.Where("member_id", memberdata.ID).Model(&models.ShowDataMember{}).First(&datamember).Error
		if errors.Is(showmember, gorm.ErrRecordNotFound) {
			return c.JSON(http.StatusNotFound, map[string]string{"message": "record not found"})
		}
		type Credetial struct {
			MemberLoginkey string `json:"member_loginkey"`
			MemberPassword string `json:"member_password"`
		}
		credential := Credetial{MemberLoginkey: databody.MemberPhone, MemberPassword: databody.MemberPassword}
		err = json.Unmarshal(body, &credential)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		requestByte, _ := json.Marshal(credential)

		reqLogin, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/login", bytes.NewBuffer(requestByte))
		reqLogin.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		resLogin, err := client.Do(reqLogin)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		var result map[string]interface{}
		err = json.NewDecoder(resLogin.Body).Decode(&result)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		response.Status = http.StatusOK
		response.Message = "success"
		response.Data = datamember
		response.Token = result
		return c.JSON(http.StatusOK, response)

	}
	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusInternalServerError, result)
}

func LoginMember(c echo.Context) error  {
	defer c.Request().Body.Close()
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	
	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))
	
	req, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/login", bytes.NewBuffer(body))
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	defer req.Body.Close()
	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(res.StatusCode, result)
}

func Logout(c echo.Context) error {
	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))

	req, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/logout", nil)
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	req.Header.Set("Authorization", c.Request().Header.Get("Authorization"))
	req.Header["x-refreshToken"] = []string{c.Request().Header.Get("x-refreshToken")}

	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	if res.StatusCode == 401 {
		reqToken, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/refreshtoken", nil)
		reqToken.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqToken.Header["x-refreshToken"] = []string{c.Request().Header.Get("x-refreshToken")}

		resToken, err := client.Do(reqToken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		body, err := ioutil.ReadAll(resToken.Body)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		type NewToken struct {
			AccessToken string `json:"accessToken"`
		}
		var newtoken NewToken
		err = json.Unmarshal(body, &newtoken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		reqLogout, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/logout", nil)
		reqLogout.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqLogout.Header.Set("Authorization", "Bearer " + newtoken.AccessToken)
		reqLogout.Header["x-refreshToken"] = []string{c.Request().Header.Get("x-refreshToken")}

		resLogout, err := client.Do(reqLogout)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		var result map[string]interface{}
		err = json.NewDecoder(resLogout.Body).Decode(&result)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		return c.JSON(http.StatusOK, result)
	}

	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)
}

func ResetPassword(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))

	req, _ := http.NewRequest(http.MethodPut, os.Getenv("ENDPOINT") + "user/resetpassword", bytes.NewBuffer(body))
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	if res.StatusCode == 200 {
			type DataReset struct {
				MemberLoginkey string `json:"member_loginkey"`
				MemberNewpassword string `json:"member_newpassword"`
			}
			var datareset DataReset
			err = json.Unmarshal(body, &datareset)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
			}
			hashedPassword, _ := helpers.Hash(datareset.MemberNewpassword)
			db := config.InitDatabase()
			var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
			if emailRegex.MatchString(datareset.MemberLoginkey) == true {
				gormreset := db.Model(&models.DataMember{}).Where("member_email = ?", datareset.MemberLoginkey).Updates(map[string]interface{}{"member_password": hashedPassword, "updated_at": time.Now()}).Error
				if errors.Is(gormreset, gorm.ErrInvalidData) {
					return c.JSON(http.StatusInternalServerError, map[string]string{"message": "unsupported data"})
				}
				var result map[string]interface{}
				err = json.NewDecoder(res.Body).Decode(&result)
				if err != nil {
					return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
				}
				return c.JSON(http.StatusOK, result)
			}
			gormreset := db.Model(&models.DataMember{}).Where("member_phone = ?", datareset.MemberLoginkey).Updates(map[string]interface{}{"member_password": hashedPassword, "updated_at": time.Now()}).Error
			if errors.Is(gormreset, gorm.ErrInvalidData) {
				return c.JSON(http.StatusInternalServerError, map[string]string{"message": "unsupported data"})
			}
			var result map[string]interface{}
			err = json.NewDecoder(res.Body).Decode(&result)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
			}
			return c.JSON(http.StatusOK, result)
	}
	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusInternalServerError, result)
}

func ChangePassword(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))

	req, _ := http.NewRequest(http.MethodPut, os.Getenv("ENDPOINT") + "user/changepassword", bytes.NewBuffer(body))
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	if res.StatusCode == 401 {
		reqToken, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/refreshtoken", nil)
		reqToken.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqToken.Header["x-refreshToken"] = []string{c.Request().Header.Get("x-refreshToken")}

		resToken, err := client.Do(reqToken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		bodyToken, err := ioutil.ReadAll(resToken.Body)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		type NewToken struct {
			AccessToken string `json:"accessToken"`
		}
		var newtoken NewToken
		err = json.Unmarshal(bodyToken, &newtoken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		reqChange, _ := http.NewRequest(http.MethodPut, os.Getenv("ENDPOINT") + "user/changepassword", bytes.NewBuffer(body))
		reqChange.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqChange.Header.Set("Authorization", "Bearer " + newtoken.AccessToken)
		resChange, err := client.Do(reqChange)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		reqMe, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/me", nil)
		reqMe.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqMe.Header.Set("Authorization", c.Request().Header.Get("Authorization"))

		resMe, err := client.Do(reqMe)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		var resultme map[string]interface{}
		err = json.NewDecoder(resMe.Body).Decode(&resultme)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		type DataBody struct {
			MemberNewpassword string `json:"member_newpassword"`
		}
		var databody DataBody
		err = json.Unmarshal(body, &databody)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		type DataChange struct {
			MemberEmail string `json:"member_email"`
			MemberNewpassword string `json:"member_newpassword"`
		}
		var datachange DataChange
		emailByte, _ := json.Marshal(resultme["data"])
		err = json.Unmarshal(emailByte, &datachange)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		changedata := DataChange{MemberEmail: datachange.MemberEmail, MemberNewpassword: databody.MemberNewpassword}
		hashedPassword, _ := helpers.Hash(databody.MemberNewpassword)
		db := config.InitDatabase()
		gormchange := db.Model(&models.DataMember{}).Where("member_email = ?", changedata.MemberEmail).Updates(map[string]interface{}{"member_password": hashedPassword, "updated_at": time.Now()}).Error
		if errors.Is(gormchange, gorm.ErrInvalidData) {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": "unsupported data"})
		}

		var resultchange map[string]interface{}
		err = json.NewDecoder(resChange.Body).Decode(&resultchange)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		return c.JSON(http.StatusOK, resultchange)
	}
	reqMe, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/me", nil)
	reqMe.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	reqMe.Header.Set("Authorization", c.Request().Header.Get("Authorization"))

	resMe, err := client.Do(reqMe)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	var resultme map[string]interface{}
	err = json.NewDecoder(resMe.Body).Decode(&resultme)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	type DataBody struct {
		MemberNewpassword string `json:"member_newpassword"`
	}
	var databody DataBody
	err = json.Unmarshal(body, &databody)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	type DataChange struct {
		MemberEmail string `json:"member_email"`
		MemberNewpassword string `json:"member_newpassword"`
	}
	var datachange DataChange
	emailByte, _ := json.Marshal(resultme["data"])
	err = json.Unmarshal(emailByte, &datachange)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	changedata := DataChange{MemberEmail: datachange.MemberEmail, MemberNewpassword: databody.MemberNewpassword}
	hashedPassword, _ := helpers.Hash(databody.MemberNewpassword)
	db := config.InitDatabase()
	gormchange := db.Model(&models.DataMember{}).Where("member_email = ?", changedata.MemberEmail).Updates(map[string]interface{}{"member_password": hashedPassword, "updated_at": time.Now()}).Error
	if errors.Is(gormchange, gorm.ErrInvalidData) {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "unsupported data"})
	}
	var resultchange map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&resultchange)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, resultchange)
}

func Me(c echo.Context) error {
	defer c.Request().Body.Close()
	var response models.Response
	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))

	req, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/me", nil)
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	req.Header.Set("Authorization", c.Request().Header.Get("Authorization"))
	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	if res.StatusCode == 401 {
		reqToken, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/refreshtoken", nil)
		reqToken.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqToken.Header["x-refreshToken"] = []string{c.Request().Header.Get("x-refreshToken")}
		resToken, err := client.Do(reqToken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		body, err := ioutil.ReadAll(resToken.Body)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		type NewToken struct {
			AccessToken string `json:"accessToken"`
		}
		var newtoken NewToken
		err = json.Unmarshal(body, &newtoken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		reqProfile, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/me", nil)
		reqProfile.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqProfile.Header.Set("Authorization", "Bearer " + newtoken.AccessToken)
		resProfile, err := client.Do(reqProfile)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		var result map[string]interface{}
		err = json.NewDecoder(resProfile.Body).Decode(&result)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		type ProfileMember struct {
			MemberEmail string `json:"member_email"`
			MemberName string `json:"member_name"`
			MemberPhone string `json:"member_phone"`
			MemberPhone2 string `json:"member_phone2"`
		}
		var profile ProfileMember
		emailByte, _ := json.Marshal(result["data"])
		err = json.Unmarshal(emailByte, &profile)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		db := config.InitDatabase()
		datamember := map[string]interface{}{}
		showmember := db.Where("member_email", profile.MemberEmail).Model(&models.ProfileMember{}).First(&datamember).Error
		if errors.Is(showmember, gorm.ErrRecordNotFound) {
			return c.JSON(http.StatusNotFound, map[string]string{"message": "record not found"})
		}

		profileByte, _ := json.Marshal(datamember)
		err = json.Unmarshal(profileByte, &profile)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		dataprofile := ProfileMember{
			MemberEmail: profile.MemberEmail,
			MemberName: profile.MemberName,
			MemberPhone: profile.MemberPhone,
			MemberPhone2: profile.MemberPhone2,
		}

		response.Status = http.StatusOK
		response.Message = "success"
		response.Data = dataprofile
		return c.JSON(http.StatusOK, response)
	}

	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	type ProfileMember struct {
		MemberEmail string `json:"member_email"`
		MemberName string `json:"member_name"`
		MemberPhone string `json:"member_phone"`
		MemberPhone2 string `json:"member_phone2"`
	}

	var profile ProfileMember
	emailByte, _ := json.Marshal(result["data"])
	err = json.Unmarshal(emailByte, &profile)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	db := config.InitDatabase()
	datamember := map[string]interface{}{}
	showmember := db.Where("member_email", profile.MemberEmail).Model(&models.ProfileMember{}).First(&datamember).Error
	if errors.Is(showmember, gorm.ErrRecordNotFound) {
		return c.JSON(http.StatusNotFound, map[string]string{"message": "record not found"})
	}

	profileByte, _ := json.Marshal(datamember)
	err = json.Unmarshal(profileByte, &profile)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	dataprofile := ProfileMember{
		MemberEmail: profile.MemberEmail,
		MemberName: profile.MemberName,
		MemberPhone: profile.MemberPhone,
		MemberPhone2: profile.MemberPhone2,
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = dataprofile
	return c.JSON(http.StatusOK, response)
}

func UpdateMember(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	timeout := time.Second * 10
	client := httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))
	db := config.InitDatabase()
	type DataBody struct {
		MemberName string `json:"member_name"`
		MemberPhone string `json:"member_phone"`
		MemberPhone2 string `json:"member_phone2"`
		MemberGender int `json:"member_gender"`
		MemberDob string `json:"member_dob"`
		MemberPhoto string `json:"member_photo"`
	}
	type BodyUpdate struct {
		MemberName string `json:"member_name"`
		MemberPhone string `json:"member_phone"`
		MemberPhone2 string `json:"member_phone2"`
	}
	var databody DataBody
	err = json.Unmarshal(body, &databody)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	var bodyupdate BodyUpdate
	err = json.Unmarshal(body, &bodyupdate)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	requestByte, _ := json.Marshal(bodyupdate)

	req, _ := http.NewRequest(http.MethodPut, os.Getenv("ENDPOINT") + "user/me", bytes.NewBuffer(requestByte))
	req.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	req.Header.Set("Authorization", c.Request().Header.Get("Authorization"))
	res, err := client.Do(req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	if res.StatusCode == 401 {
		reqToken, _ := http.NewRequest(http.MethodPost, os.Getenv("ENDPOINT") + "user/refreshtoken", nil)
		reqToken.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqToken.Header["x-refreshToken"] = []string{c.Request().Header.Get("x-refreshToken")}

		resToken, err := client.Do(reqToken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		bodyToken, err := ioutil.ReadAll(resToken.Body)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		type NewToken struct {
			AccessToken string `json:"accessToken"`
		}
		var newtoken NewToken
		err = json.Unmarshal(bodyToken, &newtoken)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		reqProfile, _ := http.NewRequest(http.MethodPut, os.Getenv("ENDPOINT") + "user/me", bytes.NewBuffer(requestByte))
		reqProfile.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqProfile.Header.Set("Authorization", "Bearer " + newtoken.AccessToken)
		resProfile, err := client.Do(reqProfile)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		reqMe, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/me", nil)
		reqMe.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
		reqMe.Header.Set("Authorization", "Bearer " + newtoken.AccessToken)

		resMe, err := client.Do(reqMe)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		var resultme map[string]interface{}
		err = json.NewDecoder(resMe.Body).Decode(&resultme)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		requestByteData, _ := json.Marshal(resultme["data"])
		requestReader := bytes.NewReader(requestByteData)
		responseDataUser, err := ioutil.ReadAll(requestReader)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		type Email struct {
			MemberEmail string `json:"member_email"`
		}
		var email Email
		err = json.Unmarshal(responseDataUser, &email)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		gormrprofile := db.Model(&models.DataMember{}).Where("member_email = ?", email.MemberEmail).Updates(map[string]interface{}{"member_name": databody.MemberName, "member_phone": databody.MemberPhone, "member_phone2": databody.MemberPhone2, "member_gender": databody.MemberGender, "member_dob": databody.MemberDob, "member_photo": databody.MemberPhoto, "updated_at": time.Now()}).Error
		if errors.Is(gormrprofile, gorm.ErrInvalidData) {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": "unsupported data"})
		}

		var result map[string]interface{}
		err = json.NewDecoder(resProfile.Body).Decode(&result)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}
		return c.JSON(http.StatusOK, result)

	}

	reqMe, _ := http.NewRequest(http.MethodGet, os.Getenv("ENDPOINT") + "user/me", nil)
	reqMe.Header.Set("Content-Type", c.Request().Header.Get("Content-Type"))
	reqMe.Header.Set("Authorization", c.Request().Header.Get("Authorization"))

	resMe, err := client.Do(reqMe)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	var resultme map[string]interface{}
	err = json.NewDecoder(resMe.Body).Decode(&resultme)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	requestByteData, _ := json.Marshal(resultme["data"])
	requestReader := bytes.NewReader(requestByteData)
	responseDataUser, err := ioutil.ReadAll(requestReader)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	type Email struct {
		MemberEmail string `json:"member_email"`
	}
	var email Email
	err = json.Unmarshal(responseDataUser, &email)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	gormrprofile := db.Model(&models.DataMember{}).Where("member_email = ?", email.MemberEmail).Updates(map[string]interface{}{"member_name": databody.MemberName, "member_phone": databody.MemberPhone, "member_phone2": databody.MemberPhone2, "member_gender": databody.MemberGender, "member_dob": databody.MemberDob, "member_photo": databody.MemberPhoto, "updated_at": time.Now()}).Error
	if errors.Is(gormrprofile, gorm.ErrInvalidData) {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "unsupported data"})
	}
	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)
}


