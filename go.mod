module jco-user-golang

go 1.15

require (
	github.com/0xAX/notificator v0.0.0-20191016112426-3962a5ea8da1 // indirect
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20171026143024-cafe2ce98974 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gojektech/heimdall/v6 v6.1.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-shellwords v1.0.10 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.11
)
