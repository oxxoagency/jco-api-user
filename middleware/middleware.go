package middleware

import (
	"github.com/labstack/echo/v4/middleware"
	"os"
)

var IsAuthenticated = middleware.JWTWithConfig(middleware.JWTConfig{
	SigningKey: []byte(os.Getenv("JWT_SECRET_KEY")),
})