package main

import (
	"jco-user-golang/config"
	"jco-user-golang/routes"
	"net/http"
	"time"
)

func main() {
	srv := &http.Server{
		Addr:         ":9000",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	srv.SetKeepAlivesEnabled(false)
	config.InitDatabase()
	e := routes.Init()
	e.Logger.Fatal(e.Start(":1323"))
}
