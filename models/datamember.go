package models

import (
	"gorm.io/gorm"
	"time"
)

type DataMember struct {
	gorm.DeletedAt
	MemberId int `json:"member_id"`
	MemberName string `json:"member_name"`
	MemberEmail string `json:"member_email"`
	MemberPhone string `json:"member_phone"`
	MemberPhone2 string `json:"member_phone2"`
	MemberPassword string `json:"member_password"`
	MemberIP string `json:"member_ip"`
	MemberMemberagent string `json:"member_useragent"`
}

type Register struct {
	ID uint `gorm:"column:member_id; primaryKey"`
	MemberId int `json:"member_id"`
	MemberName string `json:"member_name"`
	MemberEmail string `json:"member_email"`
	MemberPhone string `json:"member_phone"`
	MemberPhone2 string `json:"member_phone2"`
	MemberPassword string `json:"member_password"`
	MemberIp string `json:"member_ip"`
	MemberUseragent string `json:"member_useragent"`
	CreatedAt time.Time `json:"created_at"`
}

type DataMemberRegister struct {
	MemberId int `json:"member_id"`
	MemberName string `json:"member_name"`
	MemberEmail string `json:"member_email"`
	MemberPhone string `json:"member_phone"`
	MemberPhone2 string `json:"member_phone2"`
	MemberPassword string `json:"member_password"`
	MemberIP string `json:"member_ip"`
	MemberUseragent string `json:"member_useragent"`
	CreatedAt time.Time `json:"created_at"`
}

type ShowDataMember struct {
	gorm.DeletedAt
	MemberId int `json:"member_id"`
	MemberName string `json:"member_name"`
	MemberEmail string `json:"member_email"`
	MemberPhone string `json:"member_phone"`
	MemberPhone2 string `json:"member_phone2"`
	MemberPassword string `json:"member_password"`
	MemberIP string `json:"member_ip"`
	MemberUseragent string `json:"member_useragent"`
}

type ProfileMember struct {
	MemberEmail string `json:"member_email"`
	MemberName string `json:"member_name"`
	MemberPhone string `json:"member_phone"`
	MemberPhone2 string `json:"member_phone2"`
}

func (ProfileMember) TableName() string {
	return "data_member"
}

func (ShowDataMember) TableName() string {
	return "data_member"
}

func (Register) TableName() string {
	return "data_member"
}

func (DataMember) TableName() string {
	return "data_member"
}

func (DataMemberRegister) TableName() string {
	return "data_member"
}






