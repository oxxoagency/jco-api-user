package models

type Response struct {
	Status int `json:"status"`
	Message string `json:"message"`
	Data interface{} `json:"data"`
}

type ResponseRegister struct {
	Status int `json:"status"`
	Message string `json:"message"`
	Data interface{} `json:"data"`
	Token interface{} `json:"token"`
}
